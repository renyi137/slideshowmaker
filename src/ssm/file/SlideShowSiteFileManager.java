/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssm.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import static ssm.StartupConstants.PATH_SITES;
import static ssm.StartupConstants.PATH_SITES_IMAGES;
import static ssm.StartupConstants.PATH_SITES_SLIDESHOW;
import static ssm.StartupConstants.PATH_SITES_SLIDESHOW_STYLE;
import static ssm.file.SlideShowFileManager.SLASH;
import ssm.model.Slide;
import ssm.model.SlideShowModel;

/**
 *
 * @author yiren
 */
public class SlideShowSiteFileManager {
    SlideShowModel slides;
    public SlideShowSiteFileManager(SlideShowModel slidesToShow){
        slides=slidesToShow;
    }
    public void buildHTML() throws FileNotFoundException, IOException{
        String title=slides.getTitle();
       
        StringBuilder builder = new StringBuilder();
        Slide defaultSlide=slides.getSlides().get(0);
        builder.append("<!DOCTYPE html>");
        builder.append("<html>");
        builder.append("<head>");
        builder.append("<title>Slide Show</title>");
        builder.append("<script type=\"text/javascript\" src=\"./js/Slideshow.js\"></script>");
        builder.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"./css/slideshow_style.css\">");
        builder.append("</head>");
        builder.append("<body>");
        builder.append("<h1 id=\"title\">title</h1>");
        builder.append("<div>");
        builder.append("<img id=\"image\" src=\"\" alt=\"No image\">");
        builder.append("<h2 id=\"caption\">caption</h2>");
        builder.append("<button type=\"button\" id=\"previous\" onclick=\"previous()\" ><img id=\"icon1\" src=\"../../images/icons/Previous.png\"></button>\n" +
"        <button type=\"button\" id=\"play\" onclick=\"play()\"><img id=\"icon2\" src=\"../../images/icons/Play.png\"></button>\n" +
"        <button type=\"button\" id=\"next\" onclick=\"next()\"><img id=\"icon3\" src=\"../../images/icons/Next.png\"></button>\n" +
"        </div>\n" +
"    </body>\n" +
"</html>");
        String html = builder.toString();
        File htmlFile=new File(PATH_SITES+title+"/"+"index.html");
        FileWriter htmlWriter=new FileWriter(htmlFile);
        htmlWriter.write(html);
        htmlWriter.close();
    }
    public void buildCSS() throws IOException{
        
        FileWriter cssWriter = null;
            StringBuilder builder = new StringBuilder();
            builder.append("h1 {\n" +
"    font-size:70px;\n" +
"}\n" +
"div{\n" +
"    text-align: center;\n" +
"}\n" +
"#image{\n" +
"    height:400px;\n" +
"}\n" +
"h2 {\n" +
"    font-size:30px;\n" +
"}");
            String css = builder.toString();
            File cssFile=new File(PATH_SITES+slides.getTitle()+PATH_SITES_SLIDESHOW_STYLE);
            cssWriter = new FileWriter(cssFile);
            cssWriter.write(css);
            cssWriter.close();     
    }
    public void copyImages() throws IOException{
        for (Slide slide : slides.getSlides()){
            String imagePath=slide.getImagePath();
            String imageName=slide.getImageFileName();
            FileInputStream imageInput=new FileInputStream(imagePath+SLASH+imageName);
            FileOutputStream imageOutput=new FileOutputStream(PATH_SITES+slides.getTitle()+PATH_SITES_IMAGES+imageName);
            byte[] buf=new byte[imageInput.available()];
            imageInput.read(buf);
            imageOutput.write(buf);
            imageInput.close();
            imageInput.close();
        }
    }
    public void buildJS() throws IOException{
       FileWriter jsWriter = null;
       StringBuilder builder = new StringBuilder();
            builder.append("var xmlhttp = new XMLHttpRequest();\n" +
"var url = \"../../data/slide_shows/"+slides.getTitle()+".json\";\n" +
"var slides;\n" +
"var title;\n" +
"var caption;\n" +
"var imgName;\n" +
"var i;\n" +
"var length;\n" +
"var myTimer;\n" +
"xmlhttp.onreadystatechange = function() {\n" +
"    if (xmlhttp.readyState == 4) {\n" +
"        slides = JSON.parse(xmlhttp.responseText);\n" +
"        length=slides.slides.length;\n" +
"        i=0;\n" +
"        title= slides.title;\n" +
"        document.getElementById(\"title\").innerHTML = title;\n" +
"        caption=slides.slides[i].caption;\n" +
"        document.getElementById(\"caption\").innerHTML = caption;\n" +
"        imgName=slides.slides[i].image_file_name;\n" +
"        document.getElementById(\"image\").src = \"./img/\"+imgName;\n" +
"\n" +
"    }\n" +
"}\n" +
"xmlhttp.open(\"GET\", url, true);\n" +
"xmlhttp.send();\n" +
"\n" +
"function next() {\n" +
"    if(i+1==length){\n" +

"        i=-1;\n" +
"    }\n" +
"    i=i+1;\n" +
"    caption=slides.slides[i].caption;\n" +
"    document.getElementById(\"caption\").innerHTML = caption;\n" +
"    imgName=slides.slides[i].image_file_name;\n" +
"    document.getElementById(\"image\").src = \"./img/\"+imgName;\n" +

"}\n" +
"function previous() {\n" + 
"    if(i==0){\n" +

"        i=length;\n" +
"    }\n" +
"    i=i-1;\n" +
"    caption=slides.slides[i].caption;\n" +
"    document.getElementById(\"caption\").innerHTML = caption;\n" +
"    imgName=slides.slides[i].image_file_name;\n" +
"    document.getElementById(\"image\").src = \"./img/\"+imgName;\n" +
"}\n" +
"function play(){\n" +
"   \n" +
"    var playAndPause=document.getElementById(\"play\");\n" +
"    \n" +
"    if(playAndPause.innerHTML.match(\"Play\")){\n" +
"        \n" +
"        document.getElementById(\"icon2\").src=\"../../images/icons/Pause.png\";\n" +
"        myTimer=setInterval(next,3000);\n" +
"        \n" +
"    }\n" +
"    else\n" +
"        {\n" +
"         document.getElementById(\"icon2\").src=\"../../images/icons/Play.png\";\n" +
"         clearInterval(myTimer);\n" +
"    }\n" +
"}");
            String js = builder.toString();
            File jsFile=new File(PATH_SITES+slides.getTitle()+PATH_SITES_SLIDESHOW);
            jsWriter = new FileWriter(jsFile);
            jsWriter.write(js);
            jsWriter.close(); 
    }
    
    
   
   
}
