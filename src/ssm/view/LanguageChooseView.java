
package ssm.view;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;

import javafx.scene.layout.GridPane;

import javafx.stage.Stage;
import static ssm.StartupConstants.CSS_CLASS_SLIDE_SHOW_LANGUAGECHOOSE_BUTTON;

import static ssm.StartupConstants.CSS_CLASS_SLIDE_SHOW_SSMPANE;
import static ssm.StartupConstants.CSS_CLASS_SLIDE_SHOW_TOOLPANE;
import static ssm.StartupConstants.CSS_CLASS_VERTICAL_TOOLBAR_BUTTON;
import static ssm.StartupConstants.CSS_ClASS_SLIDE_SHOW_TEXT_STYLE;
import static ssm.StartupConstants.STYLE_SHEET_UI;

/**
 * This class provide a dialog that asks the user to choose a language from a 
 * combo box
 * 
 * @author Yi Ren
 */
public class LanguageChooseView {
    Stage primaryStage;
    Scene primaryScene;
    GridPane textPane;
    ComboBox <String> languageList;
    Label text1;
    Button OkButton;
    int choose;
    /**
     * This is a default constructor with a default choice 
     */
    public LanguageChooseView(){
        choose=1;
    }
    /**
     * Initialize the UI and offer language choice for user
     * @return language choice
     */
    public int start(){
       
        OkButton=new Button("Ok");
        OkButton.getStyleClass().add(CSS_ClASS_SLIDE_SHOW_TEXT_STYLE);
        OkButton.getStyleClass().add(CSS_CLASS_SLIDE_SHOW_LANGUAGECHOOSE_BUTTON);
        languageList =new ComboBox();
        languageList.getStyleClass().add(CSS_ClASS_SLIDE_SHOW_TEXT_STYLE);
        languageList.getStyleClass().add(CSS_CLASS_SLIDE_SHOW_TOOLPANE);
        languageList.getItems().addAll(
        "English",
        "中文");
        languageList.setValue("English");
        text1=new Label("Please choose a language");
        text1.getStyleClass().add(CSS_ClASS_SLIDE_SHOW_TEXT_STYLE);
        textPane=new GridPane();
        textPane.getStyleClass().add(CSS_CLASS_SLIDE_SHOW_SSMPANE);
        textPane.add(text1,0,0);
        textPane.add(languageList,2,0);
        textPane.add(OkButton,1,2);
        primaryScene=new Scene(textPane);
        primaryScene.getStylesheets().add(STYLE_SHEET_UI);
        primaryStage=new Stage();
        primaryStage.setWidth(350);
        primaryStage.setHeight(90);
        primaryStage.setScene(primaryScene);
        OkButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
                            if(languageList.getValue().equals("English")){
                                    choose=1;
                                }
                            else if(languageList.getValue().equals("中文")){
                                    choose=2;
                                }
                            primaryStage.close();
                                
			} 
		});
        primaryStage.showAndWait(); 
        return choose;  
    }
}
