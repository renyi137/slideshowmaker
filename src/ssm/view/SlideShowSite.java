/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssm.view;

import com.sun.javaws.Main;
import java.io.File;
import java.net.URL;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Screen;
import javafx.stage.Stage;
import static ssm.StartupConstants.PATH_SITES;
import ssm.model.SlideShowModel;

/**
 *
 * @author yiren
 */
public class SlideShowSite {
    SlideShowModel slides;
    Stage siteStage;
    Scene siteScene;
    WebView site;
    WebEngine siteEngine;
    public SlideShowSite(SlideShowModel slidesToShow){
        slides=slidesToShow;
    }
    public void startUI(){
    Screen screen = Screen.getPrimary();
    Rectangle2D bounds = screen.getVisualBounds();
    siteStage=new Stage();
    siteStage.setX(bounds.getMinX());
    siteStage.setY(bounds.getMinY());
    siteStage.setWidth(bounds.getWidth());
    siteStage.setHeight(bounds.getHeight());
    site=new WebView();
    siteEngine=site.getEngine();
    File htmlFile=new File("./sites/"+slides.getTitle()+"/index.html");
    System.out.println(htmlFile.getAbsolutePath());
    String url="file://"+htmlFile.getAbsolutePath();
    siteEngine.load(url);
    siteScene=new Scene(site);
    
    siteStage.setScene(siteScene);
    siteStage.show();
    }
}
