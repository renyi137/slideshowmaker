
package ssm.view;

import java.io.File;
import java.net.URL;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import static ssm.LanguagePropertyType.TITLE_VIEW_WINDOW;
import static ssm.LanguagePropertyType.TITLE_WINDOW;

import static ssm.LanguagePropertyType.TOOLTIP_NEXT_SLIDE;
import static ssm.LanguagePropertyType.TOOLTIP_PREVIOUS_SLIDE;
import static ssm.StartupConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON;
import static ssm.StartupConstants.DEFAULT_SLIDE_SHOW_HEIGHT;

import static ssm.StartupConstants.ICON_NEXT;
import static ssm.StartupConstants.ICON_PREVIOUS;
import static ssm.StartupConstants.PATH_ICONS;
import static ssm.StartupConstants.STYLE_SHEET_UI;
import ssm.controller.SlideShowController;
import ssm.error.ErrorHandler;
import static ssm.file.SlideShowFileManager.SLASH;
import ssm.model.Slide;
import ssm.model.SlideShowModel;

/**
 * This class provide a UI that help user view the slide show
 * 
 * @author Yi Ren
 */
public class SlideShowView{
    SlideShowModel slides;
    Slide showingSlide;
    Stage primaryStage;
    Scene primaryScene;

    // THIS PANE ORGANIZES THE BIG PICTURE CONTAINERS FOR THE
    // APPLICATION GUI
    BorderPane ssvPane;

    // THIS IS THE TOP TOOLBAR AND ITS CONTROLS
    FlowPane toolbarPane;
    Button previousSlideButton;
    Button nextSlideButton;
    ImageView imageView;
    //THIS IS THE CAPTION OF THE SLIDE
    Label caption;
    Label title;
    // PROVIDES RESPONSES FOR TOOLBAR BUTTONS
    SlideShowController showController;
    /**
     * This is a default constructor initialize the property
     * @param initslides the slides to show
     */
    public SlideShowView(SlideShowModel initslides){
        slides=initslides;
        if(!slides.isEmpty()){
            showingSlide=slides.getSlides().get(0);
        }
        primaryStage=new Stage();
        caption=new Label();
        title=new Label(slides.getTitle());
        imageView=new ImageView();
        
    }
    // UI SETUP HELPER METHODS
    private void initWorkspace() {
        toolbarPane=new FlowPane();
        previousSlideButton=this.initChildButton(toolbarPane, ICON_PREVIOUS,	TOOLTIP_PREVIOUS_SLIDE,	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
        nextSlideButton=this.initChildButton(toolbarPane, ICON_NEXT,	TOOLTIP_NEXT_SLIDE,	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        updateToolbarControls();
    }
    /**
     * Initializes the UI controls and show the slides
     */
    public void startUI(){
        initWorkspace();
        initWindow();
        initEventHandlers();
        if(showingSlide!=null){
            reloadSlide(showingSlide);
        }
     
    }
    /**
    * Initializes the UI
    **/
    private void initWindow() {
	// SET THE WINDOW TITLE
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        props.getProperty(TITLE_VIEW_WINDOW);
	primaryStage.setTitle(props.getProperty(TITLE_VIEW_WINDOW));

	// GET THE SIZE OF THE SCREEN
	Screen screen = Screen.getPrimary();
	Rectangle2D bounds = screen.getVisualBounds();

	// AND USE IT TO SIZE THE WINDOW
	primaryStage.setX(bounds.getMinX());
	primaryStage.setY(bounds.getMinY());
	primaryStage.setWidth(bounds.getWidth());
	primaryStage.setHeight(bounds.getHeight());
        
        // SETUP THE UI, NOTE WE'LL ADD THE WORKSPACE LATER
	ssvPane = new BorderPane();
        toolbarPane.getChildren().add(title);
	ssvPane.setTop(toolbarPane);
        ssvPane.setCenter(imageView);     
        ssvPane.setBottom(caption);
	primaryScene = new Scene(ssvPane);
	
        // NOW TIE THE SCENE TO THE WINDOW, SELECT THE STYLESHEET
	// WE'LL USE TO STYLIZE OUR GUI CONTROLS, AND OPEN THE WINDOW
	primaryScene.getStylesheets().add(STYLE_SHEET_UI);
	primaryStage.setScene(primaryScene);
	primaryStage.show();
    }
    /**
     * 
     */
    public void initEventHandlers(){
        showController=new SlideShowController(this);
        previousSlideButton.setOnAction(e -> {
	    showController.handlePreviousRequest(showingSlide);
	});
        nextSlideButton.setOnAction(e -> {
	    showController.handleNextRequest(showingSlide);
	});
        
    }
    /**
     * This helps initialize buttons in a toolbar, constructing a custom button
     * with a customly provided icon and tooltip, adding it to the provided
     * toolbar pane, and then returning it.
     */
    public Button initChildButton(
	    Pane toolbar, 
	    String iconFileName, 
	    LanguagePropertyType tooltip, 
	    String cssClass,
	    boolean disabled) {
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	String imagePath = "file:" + PATH_ICONS + iconFileName;
	Image buttonImage = new Image(imagePath);
	Button button = new Button();
	button.getStyleClass().add(cssClass);
	button.setDisable(disabled);
	button.setGraphic(new ImageView(buttonImage));
	Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
	button.setTooltip(buttonTooltip);
	toolbar.getChildren().add(button);
	return button;
    }
    /**
     * Show a new slide in UI
     * @param slideToLoad the slide to show
     */
    public void reloadSlide(Slide slideToLoad){
        showingSlide=slideToLoad;
        caption.setText(slideToLoad.getCaption());
        String imagePath = slideToLoad.getImagePath() + SLASH + slideToLoad.getImageFileName();
	File file = new File(imagePath);
        try {
	    // GET AND SET THE IMAGE
	    URL fileURL = file.toURI().toURL();
	    Image slideImage = new Image(fileURL.toExternalForm());
	    imageView.setImage(slideImage);
	    
	    // AND RESIZE IT
	    double scaledHeight = DEFAULT_SLIDE_SHOW_HEIGHT;
	    double perc = scaledHeight / slideImage.getHeight();
	    double scaledWidth = slideImage.getWidth() * perc;
	    imageView.setFitWidth(scaledWidth);
	    imageView.setFitHeight(scaledHeight);
	} catch (Exception e) {
	    ErrorHandler eH = slides.getUi().getErrorHandler();
            eH.processError(LanguagePropertyType.ERROR_DATA_FILE_LOADING);
	}
        showingSlide=slideToLoad;
    }
    //Accessor method
    public SlideShowModel getSlides(){
        return slides;
    }
    /**Updates the enabled/disabled status of all edit toolbar
     * buttons.
     **/
    public void updateToolbarControls(){
        int indexOfShowingSlide=slides.getSlides().indexOf(showingSlide);
        if(slides.getSize()<=1){
            previousSlideButton.setDisable(true);
            nextSlideButton.setDisable(true);
        }
        else if(indexOfShowingSlide==0){
            previousSlideButton.setDisable(true);
            nextSlideButton.setDisable(false);
        }
        else if(indexOfShowingSlide==slides.getSize()-1){
            previousSlideButton.setDisable(false);
            nextSlideButton.setDisable(true);
        }
        else {
            previousSlideButton.setDisable(false);
            nextSlideButton.setDisable(false);
        }
    }
}
