package ssm.model;

import ssm.view.SlideEditView;

/**
 * This class represents a single slide in a slide show.
 * 
 * @author McKilla Gorilla & _____________
 */
public class Slide {
    String imageFileName;
    String imagePath;
    String caption;
    SlideEditView slideEditView;
    /**
     * Constructor, it initializes all slide data.
     * @param initImageFileName File name of the image.
     * 
     * @param initImagePath File path for the image.
     * 
     */
    public Slide(String initImageFileName, String initImagePath, String initCaption) {
	imageFileName = initImageFileName;
	imagePath = initImagePath;
        caption=initCaption;
    }
    
    // ACCESSOR METHODS
    public String getImageFileName() { return imageFileName; }
    public String getImagePath() { return imagePath; }
    public SlideEditView getSlideEditView(){
        return slideEditView;
    }
    public String getCaption(){
        return caption;}
    
    // MUTATOR METHODS
    public void setImageFileName(String initImageFileName) {
	imageFileName = initImageFileName;
    }
    
    public void setImagePath(String initImagePath) {
	imagePath = initImagePath;
    }
    
    public void setImage(String initPath, String initFileName) {
	imagePath = initPath;
	imageFileName = initFileName;
    }
    public void setCaption(String newCaption){
        caption=newCaption;
    }
    
    public void setSlideEditView(SlideEditView initSlideEditView){
        slideEditView=initSlideEditView;
    }
    
}
