/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssm.controller;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static ssm.StartupConstants.PATH_CSS;
import static ssm.StartupConstants.PATH_SITES;
import static ssm.StartupConstants.PATH_SITES_CSS;
import static ssm.StartupConstants.PATH_SITES_IMAGES;
import static ssm.StartupConstants.PATH_SITES_JS;
import ssm.file.SlideShowSiteFileManager;
import ssm.model.SlideShowModel;
import ssm.view.SlideShowSite;

/**
 *
 * @author yiren
 */
public class SlideshowSiteController {
    public SlideshowSiteController()
    {}
    public void handleViewSlideShowRequest(SlideShowModel slides)
    {   File f0=new File("./sites");
        File f1=new File(PATH_SITES+slides.getTitle());
        File f2=new File(PATH_SITES+slides.getTitle()+PATH_SITES_CSS);
        File f3=new File(PATH_SITES+slides.getTitle()+PATH_SITES_JS);
        File f4=new File(PATH_SITES+slides.getTitle()+PATH_SITES_IMAGES);
        f0.mkdir();
        f1.mkdir();
        f2.mkdir();
        f3.mkdir();
        f4.mkdir();
        SlideShowSiteFileManager siteFileManager=new SlideShowSiteFileManager(slides);
        try {
            siteFileManager.copyImages();
            siteFileManager.buildCSS();
            siteFileManager.buildJS();
            siteFileManager.buildHTML();
            
        } catch (IOException ex) {
            Logger.getLogger(SlideshowSiteController.class.getName()).log(Level.SEVERE, null, ex);
        }
        SlideShowSite siteShow=new SlideShowSite(slides);
        siteShow.startUI();
 
        
    }
}
