package ssm.controller;


import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;
import static ssm.StartupConstants.CSS_CLASS_SLIDE_EDIT_SELECTED_VIEW;
import static ssm.StartupConstants.CSS_CLASS_SLIDE_EDIT_VIEW;
import static ssm.StartupConstants.DEFAULT_SLIDE_IMAGE;
import static ssm.StartupConstants.PATH_SLIDE_SHOW_IMAGES;
import ssm.model.Slide;
import ssm.model.SlideShowModel;
import ssm.view.SlideEditView;
import ssm.view.SlideShowMakerView;

/**
 * This controller provides responses for the slideshow edit toolbar,
 * which allows the user to add, remove, and reorder slides.
 * 
 * @author McKilla Gorilla & Yi Ren
 */
public class SlideShowEditController {
    // APP UI
    private SlideShowMakerView ui;
    public SlideShowEditController(){
    }
    /**
     * This constructor keeps the UI for later.
     */
    
    public SlideShowEditController(SlideShowMakerView initUI) {
	ui = initUI;
    }
    
    /**
     * Provides a response for when the user wishes to add a new
     * slide to the slide show.
     */
    public void processAddSlideRequest() {
        SlideShowModel slideShow = ui.getSlideShow();
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	slideShow.addSlide(DEFAULT_SLIDE_IMAGE, PATH_SLIDE_SHOW_IMAGES,"");
        ui.getFileController().markAsEdited();
    }
    /**
     * Provides a response for when the user select slide
     * @param slideView the selected slide view
     * @param slideShow All the slides
     */
    public void processSelectController(SlideEditView slideView, SlideShowModel 
            slideShow){
        //Set the previous selected slide unselected
        if(slideShow.isSlideSelected() && slideView!=slideShow.getSelectedSlide().
                getSlideEditView()){
            slideShow.getSelectedSlide().getSlideEditView().setSelected();
            slideShow.getSelectedSlide().getSlideEditView().getStyleClass().
                    clear();
            slideShow.getSelectedSlide().getSlideEditView().getStyleClass().
                    add(CSS_CLASS_SLIDE_EDIT_VIEW);
            slideShow.setSelectedSlide(null);
        }
        //Set the new selected slide
        slideView.setSelected();
        slideView.getStyleClass().clear();
        slideView.getStyleClass().add(CSS_CLASS_SLIDE_EDIT_SELECTED_VIEW);
        for(Slide slide:slideShow.getSlides()){
            if(slide.getSlideEditView().isSelected()){
            slideShow.setSelectedSlide(slide);
            }
        }
        int SizeOfSlideShow=slideShow.getSize();
        // REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
        // THE APPROPRIATE CONTROLS
        if(SizeOfSlideShow==1){
            slideShow.getUi().updateEditToolbarControls(3);
            }
        else if(slideShow.getSelectedSlide()==slideShow.getSlides().get(0)){
            slideShow.getUi().updateEditToolbarControls(0);
        }
        else if(slideShow.getSelectedSlide()==slideShow.getSlides().
                get(SizeOfSlideShow-1)){
            slideShow.getUi().updateEditToolbarControls(1);
        }
        else {
            slideShow.getUi().updateEditToolbarControls(2);
        }
    }
    /**
     * This method provide response for when the user want to remove the selected 
     * slide
     * @param slideShow all the slides
     */
    public void processRemoveSlideRequest(SlideShowModel slideShow) {
        slideShow.getSlides().remove(slideShow.getSelectedSlide());
        slideShow.setSelectedSlide(null);
        ui.reloadSlideShowPane(slideShow);
        ui.updateEditToolbarControls(3);
        ui.getFileController().markAsEdited();
    }
    /**
     * This method provide response for when the user change caption of a slide
     * @param slide the slide whose caption changed
     * @param captionTextField  the changed captionTextField
     */
    public void processInputChangedController(Slide slide, TextField captionTextField){
        slide.setCaption(captionTextField.getText());
        ui.getFileController().markAsEdited();
    }
    /**
     * This method provide response for when the user want to move the selected 
     * slide up
     * @param slideShow 
     */
    public void processMoveUpRequest(SlideShowModel slideShow){
        slideShow.moveUpSelectedSlide();
        ui.reloadSlideShowPane(slideShow);
        ui.getFileController().markAsEdited();
    }
    /**
     * This method provide response for when the user want to move the selected 
     * slide down
     * @param slideShow 
     */
    public void processMoveDownRequest(SlideShowModel slideShow){
        slideShow.moveDownSelectedSlide();
        ui.reloadSlideShowPane(slideShow);
        ui.getFileController().markAsEdited();
    }
}
