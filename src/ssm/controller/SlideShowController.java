
package ssm.controller;

import ssm.model.Slide;
import ssm.view.SlideShowView;



/**
 * This class serves as the controller for all tool bar operations
 * driving the next slide and previous slide
 * @author Yi Ren
 */
public class SlideShowController {
    //The App UI
    private SlideShowView ui;
    //This default constructor starts with the primary APP UI
    public SlideShowController(SlideShowView initUI){
        ui=initUI;
    }
    /**
     * This method will show the previous slide
     * @param showing the slide is to be shown
     */
    
    public void handlePreviousRequest(Slide showing){
       Slide slideToShow=ui.getSlides().getSlides().get(ui.getSlides().getSlides().
               indexOf(showing)-1);
       ui.reloadSlide(slideToShow);
       ui.updateToolbarControls();
    }
    
    /**
     * This method will show the next slide
     * @param showing the slide is to be shown
     */
    public void handleNextRequest(Slide showing){
       Slide slideToShow=ui.getSlides().getSlides().get(ui.getSlides().getSlides().
               indexOf(showing)+1);
       ui.reloadSlide(slideToShow);
       ui.updateToolbarControls();
    }
}
