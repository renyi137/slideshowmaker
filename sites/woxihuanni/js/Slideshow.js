var xmlhttp = new XMLHttpRequest();
var url = "../../data/slide_shows/woxihuanni.json";
var slides;
var title;
var caption;
var imgName;
var i;
var length;
var myTimer;
xmlhttp.onreadystatechange = function() {
    if (xmlhttp.readyState == 4) {
        slides = JSON.parse(xmlhttp.responseText);
        length=slides.slides.length;
        i=0;
        title= slides.title;
        document.getElementById("title").innerHTML = title;
        caption=slides.slides[i].caption;
        document.getElementById("caption").innerHTML = caption;
        imgName=slides.slides[i].image_file_name;
        document.getElementById("image").src = "./img/"+imgName;

    }
}
xmlhttp.open("GET", url, true);
xmlhttp.send();

function next() {
    if(i+1==length){
        i=-1;
    }
    i=i+1;
    caption=slides.slides[i].caption;
    document.getElementById("caption").innerHTML = caption;
    imgName=slides.slides[i].image_file_name;
    document.getElementById("image").src = "./img/"+imgName;
}
function previous() {
    if(i==0){
        i=length;
    }
    i=i-1;
    caption=slides.slides[i].caption;
    document.getElementById("caption").innerHTML = caption;
    imgName=slides.slides[i].image_file_name;
    document.getElementById("image").src = "./img/"+imgName;
}
function play(){
   
    var playAndPause=document.getElementById("play");
    
    if(playAndPause.innerHTML.match("Play")){
        
        document.getElementById("icon2").src="../../images/icons/Pause.png";
        myTimer=setInterval(next,3000);
        
    }
    else
        {
         document.getElementById("icon2").src="../../images/icons/Play.png";
         clearInterval(myTimer);
    }
}